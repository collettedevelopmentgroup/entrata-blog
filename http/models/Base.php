<?php
/**
 * Created by PhpStorm.
 * User: tyler
 * Date: 6/25/18
 * Time: 4:58 PM
 */

namespace http\models;


class Base
{
    protected $class = Base::class;

    public function __construct() { }

    public function __get($name)
    {
        if (!property_exists($this->class, $name)) {
            return null;
        }

        return $this->{$name};
    }

    public function __set($name, $value)
    {
        if (property_exists($this->class, $name)) {
            $this->{$name} = $value;
        }
    }
}