<?php
/**
 * Created by PhpStorm.
 * User: tyler
 * Date: 6/25/18
 * Time: 4:47 PM
 */

namespace http\models;


class User extends Base
{
    protected $id;
    protected $first_name;
    protected $last_name;

    public function __construct() {
        $this->class = User::class;
    }
}
