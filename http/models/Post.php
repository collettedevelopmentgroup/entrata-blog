<?php
/**
 * Created by PhpStorm.
 * User: tyler
 * Date: 6/25/18
 * Time: 4:55 PM
 */

namespace http\models;

class Post extends Base
{
    protected $id;
    protected $title;
    protected $content;
    protected $created_at;
    protected $created_by;
    protected $author;
    protected $comments;

    public function __construct() {
        $this->class = Post::class;
        $this->author = new User();
        $this->comments = [];
    }

    public function addComment(Comment $comment) {
        $this->comments[] = $comment;
    }
}