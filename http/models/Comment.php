<?php
/**
 * Created by PhpStorm.
 * User: tyler
 * Date: 6/25/18
 * Time: 4:56 PM
 */

namespace http\models;


class Comment extends Base
{
    protected $id;
    protected $parent_id;
    protected $post_id;
    protected $title;
    protected $content;
    protected $created_at;
    protected $comments;
    protected $level;

    public function __construct() {
        $this->class = Comment::class;
        $this->comments = [];
        $this->level = 0;
    }

    public function addComment(Comment $comment) {
        $this->comments[] = $comment;
    }
}