<!doctype html>

<html lang="en">
<head>
    <meta charset="utf-8">

    <title>Entrata Blog</title>
    <meta name="description" content="Entrata Exam Blog">
    <meta name="author" content="EntrataBlog">

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!--[if lt IE 9]>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script>
    <![endif]-->
</head>

<body>
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <h1>Posts</h1>
        </div>
    </div>
    <?php if(empty($posts)){?>
        <div class="row">
            <div class="col-xs-12">
                <h4>There are no posts.</h4>
            </div>
        </div>
    <?php } else {?>
        <?php foreach ($posts as $post) {?>
            <div class="row">
                <div class="col-xs-12">
                    <h4><a href="posts/<?= $post->id ?>"><?= $post->title ?></a></h4>
                    <h5><?= "{$post->author->first_name} {$post->author->last_name}"?></h5>
                </div>
            </div>
        <?php }?>
    <?php }?>
</div>
</body>
</html>