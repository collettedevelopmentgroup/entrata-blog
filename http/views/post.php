<!doctype html>

<html lang="en">
<head>
    <meta charset="utf-8">

    <title>Entrata Blog</title>
    <meta name="description" content="Entrata Exam Blog">
    <meta name="author" content="EntrataBlog">

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
          integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!--[if lt IE 9]>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script>
    <![endif]-->

    <script
            src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
            crossorigin="anonymous"></script>

    <style>
        div.row:first-child {
            padding-top: 20px;
        }

        .comment {
            margin-top: 10px;
        }
    </style>
</head>

<body>
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <a href="/posts">Back to Posts</a>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <h1><?= $post->title ?>
                <small>By <?= "{$post->author->first_name} {$post->author->last_name}" ?></small>
            </h1>
        </div>
        <div class="col-xs-12">
            <p><?= $post->content ?></p>
        </div>
    </div>
    <form id="mainForm" style="visibility: hidden;" method="post" action="">
        <input name="comment" type="hidden" value="">
        <input name="command" type="hidden" value="">
        <input name="postId" type="hidden" value="<?= $post->id ?>">
    </form>
    <div class="row">
        <div class="col-xs-12">
            <h2>Comments
                <button id="mainReply" data-parent-id="0" onclick="replyClickHandler(this)" type="button"
                        class="btn btn-success pull-right">Reply
                </button>
            </h2>
        </div>
        <div class="col-xs-12">
            <form data-parent-id="0" style="display: none">
                <div class="form-group">
                    <textarea class="form-control" rows="5" style="resize: vertical"></textarea>
                </div>
                <div class="form-group pull-right">
                    <button type="submit" class="btn btn-primary pull-right">Post</button>
                </div>
            </form>
        </div>
    </div>
    <?php
    function recursiveDisplay(array $comments)
    {
        if (count($comments) > 0) {
            foreach ($comments as $comment) {
                echo "<div class='row'>
                            <div class='col-xs-" . (12 - $comment->level) . " " . ($comment->level > 0 ? "col-xs-offset-{$comment->level}" : "") . " comment'>
                                <div><strong>\"{$comment->content}\"</strong></div>
                                <div>{$comment->created_at}
                                    <button type='button' data-parent-id='{$comment->id}'
                                            class='btn btn-success btn-xs'
                                            onclick='replyClickHandler(this)'>Reply</button>
                                    <button type='button' data-parent-id='{$comment->id}'
                                            class='btn btn-primary btn-xs'
                                            onclick='editClickHandler(this)'>Edit</button>
                                    <button type='button' data-parent-id='{$comment->id}'
                                            class='btn btn-danger btn-xs'
                                            onclick='deleteClickHandler(this)'>Delete</button>
                                </div>
                                <form data-parent-id='{$comment->id}' style='display: none'>
                                    <div class='form-group'>
                                        <textarea class='form-control' rows='5' style='resize: vertical'></textarea>
                                    </div>
                                    <div class='form-group pull-right'>
                                        <button type='submit' class='btn btn-primary pull-right'>Submit</button>
                                    </div>
                                </form>
                            </div>
                        </div>";

                recursiveDisplay($comment->comments);
            }
        }

        return;
    }

    ?>
    <?php recursiveDisplay($post->comments) ?>
</div>

<script type="text/javascript">
    var targetCommentId;
    var forms = $('form:not(#mainForm)');
    var mainReplyBtn = $('#mainReply');
    var mainForm = $('#mainForm');
    var command = 'reply';

    function replyClickHandler(el) {
        command = 'reply';
        clickHandler($(el));
    }

    function editClickHandler(el) {
        command = 'edit';
        clickHandler($(el));
    }

    function deleteClickHandler(el) {
        command = 'delete';
        var btn = $(el);

        mainForm.find('input[name="command"]').val(command);
        mainForm.attr('action', "/comments/" + btn.data('parentId'));
        mainForm.submit();
    }

    function clickHandler(btn) {
        forms.hide();
        mainReplyBtn.show();

        targetCommentId = btn.data('parentId');
        if (btn.attr('id') == "mainReply") {
            mainReplyBtn.hide();
        }

        $('form[data-parent-id="' + targetCommentId + '"').first().show();
    }

    forms.submit(function (event) {
        event.preventDefault();
        var form = $(this);
        var parentId = form.data('parentId');
        var comment = form.find('textarea').val();

        mainForm.find('input[name="comment"]').val(comment);
        mainForm.find('input[name="command"]').val(command);
        mainForm.attr('action', "/comments/" + parentId);
        mainForm.submit();
    });
</script>
</body>
</html>