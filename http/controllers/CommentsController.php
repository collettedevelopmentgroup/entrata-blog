<?php
/**
 * Created by PhpStorm.
 * User: tyler
 * Date: 6/26/18
 * Time: 11:14 PM
 */

namespace http\controllers;

use http\services\CommentService;

class CommentsController extends BaseController
{
    public static function post($id) {
        $service = new CommentService();
        $comment = $_REQUEST['comment'];
        $command = $_REQUEST['command'];
        $postId = $_REQUEST['postId'];

        switch ($command) {
            case 'reply':
                $result = $service->postComment($id, $postId, $comment);
                break;
            case 'edit':
                $result = $service->editComment($id, $comment);
                break;
            case 'delete':
                $result = $service->deleteComment($id);
                break;
        }

        self::redirectTo("posts/{$postId}");
    }
}