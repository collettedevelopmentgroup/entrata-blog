<?php
/**
 * Created by PhpStorm.
 * User: tyler
 * Date: 6/25/18
 * Time: 5:04 PM
 */

namespace http\controllers;

use http\services\PostService;


class PostsController extends BaseController
{
    public static function get() {
        $service = new PostService();

        $posts = $service->getAllPosts(true,false,true);

        self::view('posts', [
            'posts' => $posts
        ]);
    }

    public static function fetch($id) {
        $service = new PostService();

        $post = $service->getPost($id);

        self::view('post',[
            'post' => $post
        ]);
    }
}