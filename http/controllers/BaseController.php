<?php
/**
 * Created by PhpStorm.
 * User: tyler
 * Date: 6/26/18
 * Time: 5:50 PM
 */

namespace http\controllers;


class BaseController
{
    public static function view(string $view, array $variables = []) {
        $file = DIRECTORY_ROOT."/http/views/{$view}.php";
        if (file_exists($file)) {
            extract($variables, EXTR_OVERWRITE);
            include $file;
            exit;
        }
    }

    public static function redirectTo(string $path) {
        header("Location: {$_SERVER['REQUEST_SCHEME']}://{$_SERVER['HTTP_HOST']}/{$path}");
        exit;
    }
}