<?php
/**
 * Created by PhpStorm.
 * User: tyler
 * Date: 6/26/18
 * Time: 3:55 PM
 */

namespace http\services;


class BaseService
{
    protected $pdo;
    private $dsn;
    private $username;
    private $password;

    public function __construct()
    {
        $this->initPdo();
    }

    public function __destruct()
    {
        $this->destructPdo();
    }

    private function initPdo()
    {
        $this->dsn = "mysql:host=" . getenv('DBHOST') . ";dbname=" . getenv('DB');
        $this->username = getenv('DBUSERNAME');
        $this->password = getenv('DBPASSWORD');

        $this->pdo = new \PDO($this->dsn, $this->username, $this->password);
    }

    private function destructPdo() {
        $this->pdo = null;
    }
}