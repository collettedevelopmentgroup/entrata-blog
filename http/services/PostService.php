<?php
/**
 * Created by PhpStorm.
 * User: tyler
 * Date: 6/26/18
 * Time: 3:55 PM
 */

namespace http\services;

use http\models\Comment;
use http\models\Post;

class PostService extends BaseService
{
    public function getAllPosts(bool $withId = true, bool $withContent = false, bool $withAudit = false): array
    {
        $postObjs = [];

        $sql = "SELECT " . ($withId ? "p.id, " : "") . ($withContent ? "p.content, " : "")
            . ($withAudit ? "u.id as author_id, u.first_name as author_first_name, u.last_name as author_last_name, " : "")
            . "p.title FROM posts p "
            . ($withAudit ? "JOIN users u ON p.created_by = u.id" : "");

        try {
            $stmt = $this->pdo->query($sql);
            $posts = $stmt->fetchAll(\PDO::FETCH_OBJ);

            foreach ($posts as $post) {
                $p = new Post();

                foreach ($post as $attr => $value) {
                    if (strpos($attr, 'author') !== false) {
                        $authorAttr = substr($attr, strpos($attr, '_') + 1);
                        $p->author->{$authorAttr} = $value;
                    }

                    $p->{$attr} = $value;
                }

                $postObjs[] = $p;
            }

            return $postObjs;
        } catch (\PDOException $e) {
            return [];
        }
    }

    public function getPost(int $id): Post
    {
        $stmt = $this->pdo
            ->prepare("SELECT p.id, p.title,
                          p.content,
                          u.first_name AS author_first_name,
                          u.last_name AS author_last_name,
                          c.id AS comment_id,
                          c.parent_id AS comment_parent_id,
                          c.content AS comment_content,
                          c.created_at as comment_created_at
                        FROM posts p
                          JOIN users u ON u.id = p.created_by
                          LEFT JOIN comments c ON p.id = c.post_id
                        WHERE p.id = ?
                        ORDER BY c.created_at ASC");
        $stmt->execute(array($id));
        $results = $stmt->fetchAll(\PDO::FETCH_OBJ);

        $post = new Post();

        foreach ($results as $i => $result) {
            $comment = new Comment();

            if ($i === 0) {
                foreach ($result as $attr => $value) {
                    if (strpos($attr, 'author') !== false) {
                        $authorAttr = substr($attr, strpos($attr, '_') + 1);
                        $post->author->{$authorAttr} = $value;
                        continue;
                    }

                    if (strpos($attr, 'comment') === false) {
                        $post->{$attr} = $value;
                        continue;
                    }
                }
            }

            $comment->id = $result->comment_id;
            $comment->parent_id = $result->comment_parent_id;
            $comment->content = $result->comment_content;
            $comment->created_at = $result->comment_created_at;

            if (is_null($comment->parent_id)) {
                $post->addComment($comment);
                continue;
            }

            $this->recursiveCommentAdd($post->comments, $comment, $comment->level);
        }

        return $post;
    }

    private function recursiveCommentAdd(array $comments, Comment $comment, int $level)
    {
        $i = count($comments);
        $lvl = $level+1;

        if ($i > 0) {
            while ($i--) {
                $parent_comment = $comments[$i];
                if ($comment->parent_id === $parent_comment->id) {
                    $comment->level = $lvl > 11 ? 11 : $lvl;
                    $parent_comment->addComment($comment);
                    return;
                }


                $this->recursiveCommentAdd($parent_comment->comments, $comment, $lvl);
            }
        }

        return;
    }
}