<?php
/**
 * Created by PhpStorm.
 * User: tyler
 * Date: 6/26/18
 * Time: 11:22 PM
 */

namespace http\services;


class CommentService extends BaseService
{
    public function postComment(int $parentId, int $postId, string $comment) : bool {
        $stmt = $this->pdo->prepare(
            "INSERT INTO comments (parent_id, post_id, title, content, created_at)
              VALUES (?,?,NULL,?, now())");
        return $stmt->execute(array(
            ($parentId === 0 ? null : $parentId),
            $postId,
            $comment
        ));
    }

    public function editComment(int $id, string $comment) : bool {
        $stmt = $this->pdo->prepare(
            "UPDATE comments
                SET content = ?
                WHERE id = ?"
        );

        return $stmt->execute(array(
            $comment,
            $id
        ));
    }

    public function deleteComment(int $id) : bool {
        $idsToDelete = [$id];

        $this->recursiveDeleteIdGather($idsToDelete, $id);
        $idsToDelete = implode(",", $idsToDelete);

        $stmt = $this->pdo->prepare("DELETE FROM comments WHERE id IN ({$idsToDelete})");
        return $stmt->execute();
    }

    private function recursiveDeleteIdGather(array &$ids, int $id) {
        $stmt = $this->pdo->query("SELECT id FROM comments WHERE parent_id = {$id}");
        $results = $stmt->fetchAll(\PDO::FETCH_COLUMN, 0);

        foreach ($results as $result) {
            $this->recursiveDeleteIdGather($ids, $result);
        }

        $ids = array_merge($ids, $results);
        return;
    }
}