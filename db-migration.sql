DROP DATABASE IF EXISTS entrata_blog;
CREATE DATABASE entrata_blog;
USE entrata_blog;

CREATE TABLE users
(
  id         INT          NOT NULL AUTO_INCREMENT,
  first_name VARCHAR(100) NOT NULL,
  last_name  VARCHAR(100) NOT NULL,
  PRIMARY KEY (id)
)
  ENGINE = InnoDB;

CREATE TABLE posts
(
  id         INT          NOT NULL AUTO_INCREMENT,
  title      VARCHAR(255) NOT NULL,
  content    TEXT         NOT NULL,
  created_at DATETIME     NOT NULL,
  created_by INT          NOT NULL,
  PRIMARY KEY (id),
  INDEX posts_created_by_users_id_fk (created_by),
  FOREIGN KEY (created_by) REFERENCES users (id)
)
  ENGINE = InnoDB;

CREATE TABLE comments
(
  id         INT          NOT NULL AUTO_INCREMENT,
  parent_id  INT          NULL,
  post_id    INT          NOT NULL,
  title      VARCHAR(100) NULL,
  content    TEXT         NOT NULL,
  created_at DATETIME     NOT NULL,
  PRIMARY KEY (id),
  INDEX comments_parent_id_idx (parent_id),
  INDEX comments_post_id_posts_id_fk (post_id),
  FOREIGN KEY (post_id) REFERENCES posts (id)
)
  ENGINE = InnoDB;

INSERT INTO users (first_name, last_name)
VALUES ('Rene', 'Fleming'),
  ('Logan', 'Vasquez'),
  ('Samuel', 'Larson');

INSERT INTO posts (title, content, created_at, created_by)
VALUES
  ('Apple launches iOS 12 public beta: How it will change your iPhone and iPad',
   'The future of your iPhone or iPad is here now. Well sort of.

Apple released the public beta of iOS 12 Monday afternoon, the software that will be running at the core of the next iPhone and iPad, and, more than likely, the Apple phone and tablet you already own.

Since this is still an early pre-release beta, trying out iOS 12 now carries some risks, especially if you plan on installing the software on the Apple devices you use every day.

For starters, not all the apps you currently use may work with the public beta. You might experience bugs. Nor are all the features Apple is promising with iOS 12 available yet or fully finished. For example, the Shortcuts app that you might use to set up multistep workflows is not part of this initial public beta.

If you''re feeling brave, and not willing to wait until iOS 12''s official release come fall, head to beta.apple.com to fetch iOS 12 for free.

But first back up your iOS device before installing the public beta. If you run into a major issue, you can always restore your device to that iOS 11 backup. ',
   now(),
   1
  ),
  ('When universes collide',
  'By Mikhail Madnani Characters from BlazBlue, Persona, Under Night In-Birth, and RWBY do battle in this crossover, and the end result is near perfect.

When it comes to fighting games, most people know about Mortal Kombat, Street Fighter and Tekken, but over the last few years, Japanese developer-publisher Arc System Works has been growing in popularity thanks to fantastic games like Dragon Ball FighterZ and the Guilty Gear and BlazBlue franchises that are popular in competitive circles. Its latest - BlazBlue: Cross Tag Battle - is a fantastic fighting game and a great introduction to the developer''s other fighters.

Cross Tag Battle is a crossover fighting game with 2 versus 2 gameplay featuring characters from the BlazBlue, Persona 4 Arena, RWBY, and Under Night In-Birth franchises. Unlike some games that require precise button combinations and directional inputs for basic skills, Cross Tag Battle is all about strategising your tag attacks and executing simple and stylish combos through simple inputs. Casual fans will find the gameplay welcoming, while fighting game veterans have enough depth in tag mechanics and advanced skills.
In terms of visuals and performance, Cross Tag Battle looks and plays brilliantly on both PS4 and Nintendo Switch. The Switch version looks rougher around the edges, but that''s the only difference. On the Switch, you can even play against a friend on the same console using a single Joy-Con.

The only flaw here is that half the roster is paid DLC, which negates the game''s relatively lower price. Overall, this is an excellent and accessible fighting game. It offers tons of depth but can still be enjoyed by the most casual of fighting game players. ',
  now(),
  2),
  ('Suspects in Schererville cash register theft were in a Jeep stolen from St. John, police say',
  'SCHERERVILLE - A man tore a cash register from the counter of a gas station late Sunday, handed it to someone in a waiting vehicle and left, police said.

Schererville police on Monday released surveillance images of the man suspected in the theft about 11:45 p.m. Sunday at the Speedway, 2333 U.S. 41.

An employee told police he went to a back room and returned a short time later to find the register had been stolen. The register contained less than $100.

Surveillance video showed one black Jeep and one white Jeep pull up to two separate pumps, police said. Three people exited the Jeeps, one of whom entered the gas station.

The man hesitated briefly and jumped on the counter, leaned backward, tore an electrical cord and left with the register. He handed the register to someone in the black Jeep, and both vehicles left the area, police said.

Police later learned the white Jeep had been stolen from St. John, along with the Jeep owner''s credit cards, Schererville police Cmdr. Jeff Cook said.

The man was described as black with a gray hoodie, stone-washed jeans and black tennis shoes with white trim.

Anyone with information is asked to call Schererville police at 219-322-5000.',
  now(),
  3);

