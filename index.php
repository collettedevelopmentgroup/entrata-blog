<?php
/**
 * Created by PhpStorm.
 * User: tyler
 * Date: 6/25/18
 * Time: 4:24 PM
 */

require 'vendor/autoload.php';

define('DIRECTORY_ROOT', __DIR__);

// AUTOLOADER
spl_autoload_register(function ($class) {
    $filename = "/" . str_replace("\\", '/', $class) . ".php";
    $file = DIRECTORY_ROOT . $filename;
    if (file_exists($file)) {
        include $file;
    }
});

// DEFAULT ROUTING REROUTE
preg_match("/(^\/$|^[\/\W]{2,})/i",
    $_SERVER['REQUEST_URI'],
    $output_array);
if (!empty($output_array)) {
    header("Location: {$_SERVER['REQUEST_SCHEME']}://{$_SERVER['HTTP_HOST']}/posts");
    exit;
}

// LOAD ENV VARIABLES
$dotenv = new \Dotenv\Dotenv(DIRECTORY_ROOT);
$dotenv->load();

// BASIC ROUTING
$params = explode('/', $_SERVER['REQUEST_URI']);
array_shift($params);

if (file_exists(DIRECTORY_ROOT . "/http/controllers/" . ucwords($params[0]) . "Controller.php")) {
    switch ($_SERVER['REQUEST_METHOD']) {
        case 'GET':
            if (count($params) > 1) {
                call_user_func_array("\http\controllers\\" . ucwords($params[0]) . "Controller::fetch", [$params[1]]);
            }

            call_user_func("\http\controllers\\" . ucwords($params[0]) . "Controller::" . strtolower($_SERVER['REQUEST_METHOD']));
            break;
        case 'POST':
            call_user_func_array("\http\controllers\\" . ucwords($params[0]) . "Controller::" . strtolower($_SERVER['REQUEST_METHOD']), [$params[1]]);
            break;
    }
}